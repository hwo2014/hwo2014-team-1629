import json
import socket
import sys, math

import pprint
from datetime import datetime
from collections import defaultdict

import race

#initialise variables
mycolor = 'red'
pieces = []
blocks = {}
angle_now = 0.0
angle_prev = 0.0
pieceIndex = 0
inPieceDistance = 0
game_tick = 0
turbo = False
throttle = 1
previous_position = 0
previous_piece_length = 0
previous_velocity = 0
previous_piece_index = -1
previous_angle = 0.0
previous_radius = 0
piece_length = 0.0
circuit = 0
first_run = True
report = {}
switchLaneRequest = ''
final_lap = False
#aris
lanes_distance=[]
friction = 0.384
friction_test = True
friction_time= 35
reference_velocity = 0
car_width = 20.23
car_length = 40.234
car_guide_flag = 10.223
#extravagant
alpha_constant = 0



class NoobBot(object):
    
    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

    def msg(self, msg_type, data):
        print 'msg:', msg_type, data, game_tick
        self.send(json.dumps({"msgType": msg_type, "data": data, "gameTick": game_tick}))

    def send(self, msg):
        self.socket.send(msg + "\n")
        
    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})
                                 
    def createRace(self):
        #hockenheimring
        return self.msg("createRace", {"botId": {"name": "Zion","key": "f4G3ZKxZL96njA"},"trackName": "keimola","password": "123","carCount": 2})

    def join_man2man(self):
        return self.msg("joinRace", {"botId": {"name": "Keke2","key": "f4G3ZKxZL96njA"},"trackName": "keimola","password": "123","carCount": 2})

    def game_init(self, data):
        global pieces, blocks, lanes_distance, car_width, car_length, car_guide_flag, alpha_constant
        #pprint.pprint (data)
        pieces = data['race']['track']['pieces']
        for i in range(len(data['race']['track']['lanes'])):
            lanes_distance.append(data['race']['track']['lanes'][i]['distanceFromCenter'])
            print lanes_distance
        blocks = race.race(pieces, friction, alpha_constant)
        #get my technical data
        for i in data['race']['cars']:
            
            if i['id']['color'] == mycolor:
                car_width = i['dimensions']['width']
                car_length = i['dimensions']['length']
                car_guide_flag = i['dimensions']['guideFlagPosition']
            
        print "Car Technical Data", car_width, car_length, car_guide_flag


        self.ping()
    def turbo(self):
        global turbo, throttle
        if turbo:
            self.msg("turbo", "Patineetouu")
            turbo = False
            throttle = 1
            print "\n****TURBO MODE ON****\n"

    def on_turbo(self, data):
        

        """
        {u'turboDurationMilliseconds': 500.0,
        u'turboDurationTicks': 30,
        u'turboFactor': 3.0}
        """
        global turbo
        print "\nTURBO available\n"
        turbo = True
        pprint.pprint(data)

    def throttle(self, throttle):
        self.msg("throttle", throttle)
        #print "Throttle :", throttle 

    def ping(self):
        self.msg("ping", {})
        
    def switchLeft(self):
        
        global switchLaneRequest
        if not switchLaneRequest == 'Left':
            self.msg('switchLane', 'Left')
            switchLaneRequest = 'Left'
        
    def switchRight(self):
        
        global switchLaneRequest
        if not switchLaneRequest == 'Right':
            self.msg('switchLane', 'Right')
            switchLaneRequest = 'Right'
        
    def run(self):
        self.join()
        #self.join_man2man()
        #self.createRace()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        #get globals    
        global blocks, pieces, report, mycolor, final_lap
        global game_tick, throttle, previous_position, previous_angle, previous_radius 
        global circuit, previous_piece_length, previous_velocity, previous_piece_index
        global first_run, piece_length, friction, lanes_distance, friction_test, reference_velocity
        global car_width, alpha_constant

        next_piece_length = 0.0
        next_piece_angle = 0.0
        next_piece_radius = 0
        next_piece_switch = False
        
        #initialise posistion data
        positions = {}
        velocity = 0
        acceleration = 0 

        #get data
        
        for item in data:

            positions[item['id']['color']] =  {
                                    'car_angle': item['angle'],
                                    'piece_position' : item['piecePosition']['inPieceDistance'], 
                                    'piece_index': item['piecePosition']['pieceIndex']+1, 
                                    'lane_start': item['piecePosition']['lane']['startLaneIndex'],
                                    'lane_end': item['piecePosition']['lane']['endLaneIndex'],
                                    'lap' : item['piecePosition']['lap']
                    } 
        
        #get piece index
        piece_index = positions[mycolor]['piece_index']
        lap = positions[mycolor]['lap']
        position = positions[mycolor]['piece_position']
        angle = positions[mycolor]['car_angle']
        #print positions
        #calculate circuit
        circuit += position 

        #calculate velocity
        if previous_position > 0:
            if piece_index == previous_piece_index:
                velocity = position - previous_position
            else:
                velocity = (previous_piece_length - previous_position) + position
            acceleration = velocity - previous_velocity

        #get piece info
        piece_info = pieces[piece_index-1]
        try:
            piece_length = piece_info['length']
            piece_angle = 0.0
            piece_radius = 0
            is_corner = False
        except:
            piece_angle = piece_info['angle']
            piece_radius = piece_info['radius']
            piece_length = (abs(piece_angle) / 180) * math.pi * (piece_radius + lanes_distance[positions[mycolor]['lane_end']])
            is_corner = True
        try:
            piece_switch = piece_info['switch']
        except:
            piece_switch = False

        #check if there is next piece
        if piece_index < len(pieces)-1:
            next_piece_info = pieces[piece_index]
            
            try:
                next_piece_length = next_piece_info['length']
                next_piece_radius = 0
                next_piece_angle = 0.0
            except:
                next_piece_radius = next_piece_info['radius']
                next_piece_angle = next_piece_info['angle']
                next_piece_length = (abs(next_piece_angle) / 180) * math.pi * next_piece_radius
                
            try:
                next_piece_switch = next_piece_info['switch']
            except:
                next_piece_switch = False
        
        

        def build_block_by_index(index):
            #global blocks 
            #pprint.pprint (blocks)

            blocks_by_index = {}
            if not index:
                return None

            for block in blocks:
                pieces = block['pieces']
                #print pieces
                for piece in pieces:
                    #print piece
                    blocks_by_index[piece['piece_count']] = block
            
            return blocks_by_index[index]

        #find current 
        #print '\n', piece_index, len(pieces), '\n'
        block = build_block_by_index(piece_index)
        #pprint.pprint (block)
        block_velocity = block['velocity']

        block_angle = block['angle']
        block_radius = block['radius']
        block_length = block['length']

        #find next block
        last_piece_count = block['pieces'][len(block['pieces'])-1]['piece_count']

        #if is not last piece get next block
        if last_piece_count < len(pieces)-1:
            next_block = build_block_by_index(last_piece_count+1)
            
        else:
            #get first block info
            next_block = build_block_by_index(1)
            if lap == 2:
                final_lap = True
           


        next_block_velocity = next_block['velocity']
        next_block_angle = next_block['angle']
        next_block_radius = next_block['radius']

        def get_piece_by_index(index):
            piece_by_index = {}
            if not index:
                return None

            for piece in block['pieces']:
                #print piece
                piece_by_index[piece['piece_count']] = piece
            
            return piece_by_index[index]

        block_piece = get_piece_by_index(piece_index)
        absolute_position = (block_piece['block_length'] - piece_length) + position
        #pprint.pprint(block_piece)
        
                
        def reduce_speed(value):
            global throttle
            throttle -= value
            if throttle < 0:
                throttle = 0.1

            return throttle

        def increase_speed(value):
            global throttle
            throttle += value
            if throttle > 1:
                throttle = 1

            return throttle 
        
        def switch_ahead(piece_index, offset):
            offset = piece_index-1+offset
            if offset < len(pieces)-1:
                try: 
                    return pieces[offset]['switch']
                except:
                    return False

        def angle_ahead(piece_index, offset):
            offset = piece_index-1+offset
            if offset < len(pieces)-1:
                try: 
                    return pieces[offset]['angle']
                except:
                    return 0.0

            
        if turbo:
            if (block_length - absolute_position > 200) and not is_corner:
                self.turbo()

        if switch_ahead(piece_index, 1):
            if abs(next_block_angle) > abs(angle_ahead(piece_index, 2)):
                if next_block_angle > 0:
                    self.switchRight()
                else:
                    self.switchLeft()
            elif abs(next_block_angle) < abs(angle_ahead(piece_index, 2)):
                if abs(angle_ahead(piece_index, 2)) >abs(angle_ahead(piece_index, 3)):
                    if angle_ahead(piece_index, 2) > 0:
                        self.switchRight()
                    else:
                        self.switchLeft()
                else:
                    if angle_ahead(piece_index, 3) > 0:
                        self.switchRight()
                    else:
                        self.switchLeft()
            

        else:
            if angle_ahead(piece_index, 2) > 0:
                self.switchRight()
            elif angle_ahead(piece_index, 2) < 0:
                self.switchLeft()


        block_outer_velocity = block_velocity
        if abs(block_angle) > 0:
            block_outer_velocity = math.sqrt(friction * (block_radius + lanes_distance[positions[mycolor]['lane_end']]))
            #if (positions[mycolor]['lane_start'] == 0 and block_angle>0) or (positions[mycolor]['lane_start'] == 1 and block_angle<0):   
            if velocity > block_velocity and velocity > block_outer_velocity: #and absolute_position < block_length/2:
                #reduce_speed(0.1*(velocity/block_velocity))
                throttle = 0
            #add apex point acceleration
            #elif absolute_position > block_length/2 and next_piece_radius == 0:
            #    increase_speed(0.002)
            else:
                #throttle = 1
                #increase_speed(0.555)
                increase_speed(0.1*(abs(velocity)/next_block_velocity))
                #if next_block_angle > 0 and block_angle < 0 or next_block_angle < 0 and block_angle > 0 and next_piece_radius < 20:
                #    reduce_speed(0.1)
            #if absolute_position > block_length*13/16 and next_block_velocity > block_velocity:
            #    throttle = 1
        else:
            #9/16
            if velocity > next_block_velocity or velocity > block_velocity:
                reduce_speed(0.1*(velocity/block_velocity))
                '''
                if not final_lap:
                    reduce_speed(0.1*(velocity/block_velocity))
                    #throttle = 0 
                else:
                    throttle = 1
                '''
                #throttle = 0
                if absolute_position >= 0.4 * block_length / friction * (velocity**2 - next_block_velocity**2):
                    throttle = 0
                    print 'Stopping Distance with velocity %f: %f'%(velocity, 0.4 * block_length / friction * (velocity**2 - next_block_velocity**2))
                    #reduce_speed(0.5)
            else:
                #increase_speed(0.7)
                throttle = 1

        
        #if switch_ahead(piece_index, 1):
        '''
        if game_tick>1:
            print '\n\nVelocity', velocity, 'accelaration:', acceleration, 'absolute position:', absolute_position, "Car drift angle", angle  
            print 'Block V:', block_velocity, 'Outter V:',block_outer_velocity, 'Next Block V:',next_block_velocity
            print 'Piece:', piece_index, 'Angle:', block_angle, 'Next Angle:', next_block_angle, 'Next Piece Switch:', next_piece_switch 
            print 'Block length:', block_length, 'Lap:', lap, 'Lane Start:', positions[mycolor]['lane_start'], 'Lane End:', positions[mycolor]['lane_end']
        '''

        """ 
        if piece_length > 0:
            if next_piece_length == 0:
                    throttle -= 0.15
                    if throttle <= 0.3500:
                        throttle = 0.3954 
            else:
                throttle = 1
        else:
            if next_piece_length == 0:
                if abs(next_piece_angle) < 45.0:
                    throttle += 0.3
                    if throttle > 1:
                        throttle = 1
                else:
                    if velocity > 6:
                        throttle -= 0.1
                        if throttle < 0:
                            throttle = 0.35

                    elif velocity < 6:
                        throttle += 0.1
                        if throttle > 1:
                            throttle = 0.59999999

                    #throttle -= 0.5
                    #if throttle <= 0.3600:
                    #    throttle = 0.3600


            if next_piece_length > 0:
                throttle = 1
        
        """

        #if previous_position > 0:
        dict_key = str(lap) + '_' + str(piece_index)
        if not piece_index == previous_piece_index or first_run :
            
            report[dict_key] = {
                
                "next" : {
                            "length" : next_piece_length,
                            "switch" : str(next_piece_switch),
                            "radius" : next_piece_radius,
                            "angle" : next_piece_angle
                },
                "previous" : {
                            
                            "length" : previous_piece_length,
                            "radius" : previous_radius,
                            "angle" : previous_angle
                },
                "current" : {
                            "length" : piece_length,
                            "switch" : str(piece_switch),
                            "radius" : piece_radius,
                            "angle" : piece_angle,
                            "ticks" : []
                            }

            }
            """
            print '\n'
            print 'Piece Length:',piece_length, ' /Switch:',piece_switch, ' /Radius:',piece_radius, ' /Angle:',piece_angle
            print 'Next Length:',next_piece_length, ' /Next Switch:',next_piece_switch, ' /Next Radius:',next_piece_radius, 'Next Angle:',next_piece_angle
            print 'Previous Length:', previous_piece_length, ' /Previous position:', previous_position
            """
        else:
            report[dict_key]["current"]["ticks"].append({
                "tick" : game_tick,
                "throttle": throttle,
                "velocity" : velocity,
                "acceleration" : acceleration,
                "position" : position,
                "distance" : circuit})
            """
            print '   \nVelocity:', velocity, ' /Acceleration:',acceleration, ' /Distance:',circuit
            print '   Game tick:',game_tick, ' /Throttle:',throttle, ' /Piece postion:', position, 'Laps:',lap
            """
        #pp = pprint.PrettyPrinter(indent=4)
        #pp.pprint(report)
        #print 'tick:', game_tick, datetime.hour(datetime.now()),':',datetime.minute(datetime.now())
        #set variables
        previous_position = position
        previous_piece_length = piece_length
        previous_velocity = velocity
        previous_angle = piece_angle
        previous_radius = piece_radius 
        previous_piece_index = piece_index
        previous_is_corner = is_corner
        
        def calculate_friction():
            global reference_velocity, throttle, alpha_constant
            if game_tick < 5:
                if velocity >= reference_velocity:
                    reference_velocity = velocity
                    print "ref :", reference_velocity
                    print "vel :", velocity
            elif game_tick > 5 and game_tick < friction_time + 1:
                throttle = 0
                print "!!!!! brakes !!!!!!!"
            elif game_tick == friction_time + 1:
                print "ref :", reference_velocity
                print "vel :", velocity
                friction = (reference_velocity**2 - velocity**2)/absolute_position
                if friction > 0:
                #print "Velocity Diff: ", reference_velocity - velocity
                #print "Distance Travelled: ", circuit
                    print "Friction Calculation : ", friction
                    blocks = race.race(pieces, friction, alpha_constant)
                    friction_test = False
                else:
                    friction = 0.34901324
                    
        
        if friction_test:
            calculate_friction()
        
        #set throttle
        self.throttle(throttle)
        first_run = False

    def on_crash(self, data):
        print("Someone crashed")
        return False
        self.throttle(1)

    def on_game_end(self, data):
        print("Race ended")
        #global report
        #pprint.pprint(report)
        
        json_report = json.dumps(report)
        file = open("race.json", "w")
        file.write(json_report)
        file.close()
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        
        self.ping()
    
    def your_car(self, data):
        global mycolor
        mycolor = data['color']
        
        self.ping()


    def msg_loop(self):
        #get globals
        global game_tick

        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'yourCar': self.your_car,
            'gameInit': self.game_init,
            'turboAvailable': self.on_turbo
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            
            if msg_type in msg_map:
                '''
                if msg_type == 'crash':
                    print "Crash" 
                    #global report
                    #pp = pprint.PrettyPrinter(indent=4)
                    #pp.pprint(report)
                    break
                '''    
                #process car positions
                if msg_type == 'carPositions':
                    try:
                        game_tick = msg['gameTick']
                    except:
                        game_tick = 0

                msg_map[msg_type](data)
            else:
                                    
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()



