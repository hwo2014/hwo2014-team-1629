import json
import socket
import sys, math

import pprint
from datetime import datetime
from collections import defaultdict

def race(pieces, friction, alpha_constant):

    """
    pieces = [   {   
            u'length': 100.0},
        {   u'length': 100.0},
        {   u'length': 100.0},
        {   u'length': 100.0, u'switch': True},
        {   u'angle': 45.0, u'radius': 100},
        {   u'angle': 45.0, u'radius': 100},
        {   u'angle': 45.0, u'radius': 100},
        {   u'angle': 45.0, u'radius': 100},
        {   u'angle': 22.5, u'radius': 200, u'switch': True},
        {   u'length': 100.0},
        {   u'length': 100.0},
        {   u'angle': -22.5, u'radius': 200},
        {   u'length': 100.0},
        {   u'length': 100.0, u'switch': True},
        {   u'angle': -45.0, u'radius': 100},
        {   u'angle': -45.0, u'radius': 100},
        {   u'angle': -45.0, u'radius': 100},
        {   u'angle': -45.0, u'radius': 100},
        {   u'length': 100.0, u'switch': True},
        {   u'angle': 45.0, u'radius': 100},
        {   u'angle': 45.0, u'radius': 100},
        {   u'angle': 45.0, u'radius': 100},
        {   u'angle': 45.0, u'radius': 100},
        {   u'angle': 22.5, u'radius': 200},
        {   u'angle': -22.5, u'radius': 200},
        {   u'length': 100.0, u'switch': True},
        {   u'angle': 45.0, u'radius': 100},
        {   u'angle': 45.0, u'radius': 100},
        {   u'length': 62.0},
        {   u'angle': -45.0, u'radius': 100, u'switch': True},
        {   u'angle': -45.0, u'radius': 100},
        {   u'angle': 45.0, u'radius': 100},
        {   u'angle': 45.0, u'radius': 100},
        {   u'angle': 45.0, u'radius': 100},
        {   u'angle': 45.0, u'radius': 100},
        {   u'length': 100.0, u'switch': True},
        {   u'length': 100.0},
        {   u'length': 100.0},
        {   u'length': 100.0},
        {   u'length': 90.0}]
    """

    previous_length = 0.0
    previous_angle = 0.0
    previous_radius = 0
    block_length = 0.0
    block_angle = 0.0
    block_radius = 0.0
    blocks = []
    first_run=True
    previous_is_corner = False
    piece_count = 1 
    piece_index = [] 
    #friction = 1

    #print pieces
    for piece in pieces:
        #print piece_count 
        try:
            length = piece['length']
            angle = 0.0
            radius = 0
            is_corner = False
        except:
            angle = piece['angle']
            radius = piece['radius']
            length = (abs(angle) / 180) * math.pi * radius
            is_corner = True
        try:
            switch = piece['switch']
        except:
            switch = False
        
        #print block_length, is_corner, previous_is_corner, length, previous_length, angle, previous_angle, radius, previous_radius
        #print is_corner, previous_is_corner, block_length

        if not first_run:

            if piece_count == len(pieces):
                if not previous_is_corner and not is_corner:
                    block_length = block_length + length
                    piece_index.append({'piece_count': piece_count,'block_length': block_length, 'switch': switch })
                    velocity = 12
                else:

                    block_length = (abs(angle) / 180) * math.pi * radius
                    piece_index.append({'piece_count': piece_count,'block_length': block_length, 'switch': switch })
                    block_angle = angle
                    block_radius = radius
                    '''
                    if previous_angle < 0 and block_angle > 0 or previous_angle > 0 and block_angle < 0 and block_radius < 40:
                        velocity = velocity / 2
                    '''

                blocks.append({'velocity' : velocity,'length' : block_length, 'radius': block_radius, 'angle' : block_angle, 'pieces' : piece_index })
                #print 'New block velocity', velocity, 'legth', block_length, ', radius', block_radius, ', angle',block_angle, 'pieces, ', piece_index 

            elif  not (length == previous_length) or not (angle == previous_angle) or not (radius == previous_radius):
                
                if previous_is_corner:
                    #arc
                    block_length = (abs(block_angle) / 180) * math.pi * previous_radius
                    #velocity
                    velocity = math.sqrt((friction + alpha_constant)*previous_radius)
                    '''
                    if previous_angle < 0 and block_angle > 0 or previous_angle > 0 and block_angle < 0 and block_radius < 40:
                        velocity = velocity / 2
                    '''


                else:
                    #block_length = block_length + length
                    velocity = 12

                blocks.append({'velocity' : velocity,'length' : block_length, 'radius': block_radius, 'angle' : block_angle, 'pieces' : piece_index })
                #print 'New block velocity', velocity, 'legth', block_length, ', radius', block_radius, ', angle',block_angle, 'pieces, ', piece_index 

                piece_index = []
                block_length = length
                block_angle =  angle
                block_radius = radius
                piece_index.append({'piece_count': piece_count,'block_length': block_length, 'switch': switch })
                #print blocks 
            else:
                
                #update block
                block_length = block_length + length
                block_angle = block_angle + angle
                block_radius = radius    
                piece_index.append({'piece_count': piece_count,'block_length': block_length, 'switch': switch })
                #print 'New block size', block_length
            
        else:
            #print 'First run'
            block_radius = radius
            block_angle = angle
            block_length = length
            piece_index.append({'piece_count': piece_count,'block_length': block_length, 'switch': switch })

        #set variables to check previous
        first_run = False
        previous_length = length
        previous_angle = angle
        previous_radius = radius
        previous_is_corner = is_corner
        piece_count = piece_count + 1 

        
    #print ''
    #pprint.pprint(blocks)

    return blocks

#race()